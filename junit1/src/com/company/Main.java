package com.company;

class Main{

    // Driver code

    /**
     * compute a numer is sparse or not
     * @param inputNumber
     * @return
     */
    public boolean isSparse(String inputNumber)
    {
        for(int j=0;j<inputNumber.length();j++) {
            if (!Character.isDigit(inputNumber.charAt(j))) throw new NumberFormatException("Please Enter a Valid NUmber");
        }
        int intNumber = Integer.parseInt(inputNumber);
        intNumber = (intNumber & intNumber>>1);
        return intNumber==0;
    }
}
